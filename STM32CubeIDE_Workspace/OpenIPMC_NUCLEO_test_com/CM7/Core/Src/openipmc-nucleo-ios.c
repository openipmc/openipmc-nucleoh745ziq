/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/


/** 
 * @file openipmc-nucleo-ios.c
 * 
 * 
 */ 

// FreeRTOS includes
#include <stdio.h>
#include <stdlib.h>
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

// STM32H7xx HAL includes
#include "main.h"
#include "cmsis_os.h"

// OpenIPMC includes
#include "../openipmc/src/ipmc_ios.h"
#include "../openipmc/src/ipmb_0.h"
#include "../openipmc/src/ipmi_msg_manager.h"
#include "../openipmc/src/fru_state_machine.h"
#include "../openipmc/src/ipmc_tasks.h"

// printf lib include
#include "../printf/printf.h"

#define ACK_CHECK_EN 0x1       /*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS 0x0      /*!< I2C master will not check ack from slave */

#define I2C_SCLK_RATE   100000 // 100 kHz I2C bus
#define I2C_MODE_MASTER 1
#define I2C_MODE_SLAVE  0
#define IPMB_BUFF_SIZE  32

static _Bool ipmc_ios_ready_flag = pdFALSE;

// I2C state control variables in OpenIPMC HAL
static uint8_t i2c1_current_state;
static uint8_t i2c4_current_state;

// Length of the the received message
static uint32_t i2c1_recv_len = 0;  
static uint32_t i2c4_recv_len = 0;

// IPMB Hardware Address
static uint8_t  ipmb_addr;

//Buffers for IPMB receiving
uint8_t ipmba_input_buffer[IPMB_BUFF_SIZE] = {0};
uint8_t ipmbb_input_buffer[IPMB_BUFF_SIZE] = {0};

// Semaphores to synchronize the IPMB operations with the I2C transmission
static SemaphoreHandle_t ipmb_rec_semphr = NULL;
static SemaphoreHandle_t ipmba_send_semphr = NULL;
static SemaphoreHandle_t ipmbb_send_semphr = NULL;

// Mutex to avoid printf superpositioning.
static SemaphoreHandle_t printf_mutex = NULL;

// stm32h7xx I2C peripheral struct
I2C_HandleTypeDef hi2c1;
I2C_HandleTypeDef hi2c4;


/*
 * Initialization of IPMC peripherals in the context of ESP32 setup
 */
int periphs_init(void)
{
    int status_global = 1;
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOD_CLK_ENABLE();
    __HAL_RCC_GPIOE_CLK_ENABLE();
    __HAL_RCC_GPIOF_CLK_ENABLE();
    __HAL_RCC_GPIOG_CLK_ENABLE();
    __HAL_RCC_GPIOH_CLK_ENABLE();

        
    // Configure GPIO pin for PICMG BLUE LED - PE3
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_3, GPIO_PIN_RESET);
    GPIO_InitStruct.Pin = GPIO_PIN_3;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);
    
    // Configure GPIO pin for PICMG HANDLE SWITCH - PE6
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_6, GPIO_PIN_RESET);
    GPIO_InitStruct.Pin = GPIO_PIN_6;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);
    
    // Configure GPIO pin for PICMG HA0 - PE5
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_5, GPIO_PIN_RESET);
    GPIO_InitStruct.Pin = GPIO_PIN_5;
    HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);
    
    // Configure GPIO pin for PICMG HA1 - PE4
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_4, GPIO_PIN_RESET);
    GPIO_InitStruct.Pin = GPIO_PIN_4;
    HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);
    
    // Configure GPIO pin for PICMG HA2 - PE2
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_2, GPIO_PIN_RESET);
    GPIO_InitStruct.Pin = GPIO_PIN_2;
    HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);
    
    // Configure GPIO pin for PICMG HA3 - PD3
    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_3, GPIO_PIN_RESET);
    GPIO_InitStruct.Pin = GPIO_PIN_3;
    HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
    
    // Configure GPIO pin for PICMG HA4 - PD4
    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_4, GPIO_PIN_RESET);
    GPIO_InitStruct.Pin = GPIO_PIN_4;
    HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);    
    
    // Configure GPIO pin for PICMG HA5 - PD5
    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_5, GPIO_PIN_RESET);
    GPIO_InitStruct.Pin = GPIO_PIN_5;
    HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

    // Configure GPIO pin for PICMG HA6 - PD6
    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_6, GPIO_PIN_RESET);
    GPIO_InitStruct.Pin = GPIO_PIN_6;
    HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);    
    
    // Configure GPIO pin for PICMG HA7 - PD7
    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_7, GPIO_PIN_RESET);
    GPIO_InitStruct.Pin = GPIO_PIN_7;
    HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
    
    // Create the semaphores and mutex
    printf_mutex = xSemaphoreCreateMutex();
    ipmb_rec_semphr = xSemaphoreCreateBinary();
    ipmba_send_semphr = xSemaphoreCreateBinary();
    ipmbb_send_semphr = xSemaphoreCreateBinary();
  
    // Read Hardware address
    uint16_t hardware_addr = ipmc_ios_read_haddress();

    // initialize I2C1 in stm32h7xx - IPMB-A
    hi2c1.Instance = I2C1;
    hi2c1.Init.Timing = 0x10707DBC;
    hi2c1.Init.OwnAddress1 = (0x300 | hardware_addr << 1);
    hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
    hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
    hi2c1.Init.OwnAddress2 = 0;
    hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
    hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
    hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_ENABLE;
    if (HAL_I2C_Init(&hi2c1) != HAL_OK)
    {
        status_global = 0;
    }
    /** Configure Analog filter
    */
    if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
    {
        status_global = 0;
    }
    /** Configure Digital filter
    */
    if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
    {
        status_global = 0;
    }
    
    // initialize I2C4 in stm32h7xx - IPMB-B
    hi2c4.Instance = I2C4;
    hi2c4.Init.Timing = 0x10707DBC;
    hi2c4.Init.OwnAddress1 = (0x300 | hardware_addr << 1);
    hi2c4.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
    hi2c4.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
    hi2c4.Init.OwnAddress2 = 0;
    hi2c4.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
    hi2c4.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
    hi2c4.Init.NoStretchMode = I2C_NOSTRETCH_ENABLE;
    if (HAL_I2C_Init(&hi2c4) != HAL_OK)
    {
        status_global = 0;
    }
    /** Configure Analog filter
    */
    if (HAL_I2CEx_ConfigAnalogFilter(&hi2c4, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
    {
        status_global = 0;;
    }
    /** Configure Digital filter
    */
    if (HAL_I2CEx_ConfigDigitalFilter(&hi2c4, 0) != HAL_OK)
    {
        status_global = 0;
    }
    
   /*
    * Check if I2C peripherals are initialized
    *     pins used:
    *     PB8 - SCL I2C1
    *     PB9 - SDA I2C1
    * 
    *     PF14 - SCL I2C4
    *     PF15 - SDA I2C4
    */
    if (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
        status_global = 0; // config fail

    if (HAL_I2C_GetState(&hi2c4) != HAL_I2C_STATE_READY)
    	status_global = 0; // config fail
    	
    // begin I2C1 in SLAVE MODE
    if(HAL_I2C_Slave_Receive_IT(&hi2c1, &ipmba_input_buffer[0], IPMB_BUFF_SIZE) != HAL_OK)
        status_global = 0; // config fail
    else
    {
        if (HAL_I2C_GetMode(&hi2c1) == HAL_I2C_MODE_SLAVE)
            i2c1_current_state = I2C_MODE_SLAVE;
    }
        
    if(HAL_I2C_Slave_Receive_IT(&hi2c4, &ipmbb_input_buffer[0], IPMB_BUFF_SIZE) != HAL_OK)
        status_global = 0; // config fail
    else
    {
        if (HAL_I2C_GetMode(&hi2c4) == HAL_I2C_MODE_SLAVE)
            i2c4_current_state = I2C_MODE_SLAVE;
    }
        
    // initialization status for the IOs
    if (status_global == 1)
    	ipmc_ios_ready_flag = pdTRUE;

    /*
    * If any of the previous initialization fail, return 0.
    * Otherwise return 1 (all initialization successful)
    */
    return status_global;  
}

/*
 * Check if the IOs are initialized.
 * 
 * Returns TRUE if ipmc_ios_init() ran all the initializations successfully.
 */
_Bool ipmc_ios_ready(void)
{
	return ipmc_ios_ready_flag;
}

/*
 * Read the state of the ATCA handle
 * 
 * Mechanically, when the handle is CLOSED, the pin is grounded, giving a LOW.
 * When the handle is OPEN, the micro-switch is also open, and a pull-up 
 * resistor imposes a HIGH.
 * 
 * return value:
 *   1: handle is OPEN
 *   0: handle is CLOSED
 */
int ipmc_ios_read_handle(void)
{
  
  if( HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_6) == GPIO_PIN_SET )
    return 1; // OPEN
  else
    return 0; // CLOSED

}


/*
 * The Hardware Address of the IPMC (slave addr) is read from the ATCA backplane pins
 * A party check is considered for the address (parity must be ood)
 */
uint8_t ipmc_ios_read_haddress(void)
{
  int i;
  uint8_t  HA_bit[8] = {0, 0, 0, 0, 0, 0, 0, 0};
  uint8_t  HA_num;
  int parity_odd;
  
  /* Get the HA0 from the bit 1 in PL GPIO */
  if( HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_5) == GPIO_PIN_SET )
    HA_bit[0] = 1;
  
  /* Get HA1 to HA7 from PS GPIO1 */ 
  if( HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_4) == GPIO_PIN_SET ) // HA1
    HA_bit[1] = 1;
  if( HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_2) == GPIO_PIN_SET ) // HA2
    HA_bit[2] = 1;
  if( HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_3) == GPIO_PIN_SET ) // HA3
    HA_bit[3] = 1;
  if( HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_4) == GPIO_PIN_SET ) // HA4
    HA_bit[4] = 1;         
  if( HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_5) == GPIO_PIN_SET ) // HA5
    HA_bit[5] = 1;         
  if( HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_6) == GPIO_PIN_SET ) // HA6
    HA_bit[6] = 1;         
  if( HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_7) == GPIO_PIN_SET ) // HA7
    HA_bit[7] = 1;
  
  return 0x43; // THIS IS ONLY FOR TESTING (skips parity check, return hard coded addr)
  
  /* Calculate parity */
  parity_odd = 0; // initialize as EVEN
  for(i=0; i<8; i++)
    if(HA_bit[i] == 1)
      parity_odd = ~parity_odd; // flip parity
  
  /* Result */
  HA_num = 0;
  if( parity_odd )
  {
    for(i=0; i<=6; i++)
      HA_num |= (HA_bit[i]<<i);
    
    return HA_num; // 7bit addr
  }
  else
    return HA_PARITY_FAIL; //parity fail (must be ODD)
}


/*
 * Set the IPMB address.
 */
void ipmc_ios_ipmb_set_addr(uint8_t addr)
 {
    // Convert into 7-bit format to be used by the I2C driver
    ipmb_addr = addr >> 1;

 }

 
/*
 * Master transmit function for IPMB-A
 * IPMB-A uses STM32H7xx I2C1 - set on pins Pxx as SCL and Pxx as SDA
 */
int ipmc_ios_ipmba_send(uint8_t *MsgPtr, int ByteCount)
{
    _Bool semphr_timeout;
    uint16_t dest_addr;
    HAL_StatusTypeDef tx_ret_val;
    
    dest_addr = (uint16_t)MsgPtr[0]; // Address already shifted
    
    // must reconfigure the I2C peripheral before attempting to transmit in master mode
    HAL_I2C_DeInit(&hi2c1);
    HAL_I2C_Init(&hi2c1);
	HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE);
	HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0);

    // set current mode as Master
    i2c1_current_state = I2C_MODE_MASTER;
    
    // begin the transmission
    tx_ret_val = HAL_I2C_Master_Transmit_IT(&hi2c1, dest_addr, &MsgPtr[1], (uint16_t) ByteCount -1);
    
    // Wait for transmission to finish or timeout
    semphr_timeout = xSemaphoreTake (ipmba_send_semphr, pdMS_TO_TICKS(100));
    
    // return I2C to Slave mode
    i2c1_recv_len = 0;
    HAL_I2C_DeInit(&hi2c1);
    HAL_I2C_Init(&hi2c1);
    HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE);
    HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0);
    HAL_I2C_Slave_Receive_IT(&hi2c1, &ipmba_input_buffer[0], IPMB_BUFF_SIZE);
    i2c1_current_state = I2C_MODE_SLAVE;
    
    if ( (tx_ret_val == HAL_OK) && (semphr_timeout != pdFALSE) )
        return IPMB_SEND_DONE;
    else
    	return IPMB_SEND_FAIL;
}


/*
* Master transmit function for IPMB-B
* IPMB-B uses STM32H7xx I2C4 - set on pins Pxx as SCL and Pxx as SDA
*/
int ipmc_ios_ipmbb_send(uint8_t *MsgPtr, int ByteCount)
{
	_Bool semphr_timeout;
    uint16_t dest_addr;
    HAL_StatusTypeDef tx_ret_val;
    
    dest_addr = (uint16_t)MsgPtr[0]; // Address already shifted
    
    // must reconfigure the I2C peripheral before attempting to transmit in master mode
    HAL_I2C_DeInit(&hi2c4);
    HAL_I2C_Init(&hi2c4);
	HAL_I2CEx_ConfigAnalogFilter(&hi2c4, I2C_ANALOGFILTER_ENABLE);
	HAL_I2CEx_ConfigDigitalFilter(&hi2c4, 0);

    // set current mode as Master
    i2c4_current_state = I2C_MODE_MASTER;
    
    // begin the transmission
    tx_ret_val = HAL_I2C_Master_Transmit_IT(&hi2c4, dest_addr, &MsgPtr[1], (uint16_t) ByteCount -1);
    
    // Wait transmission finish or timeout
    semphr_timeout = xSemaphoreTake ( ipmbb_send_semphr, pdMS_TO_TICKS(100) );
    
    // return I2C to Slave mode
    i2c4_recv_len = 0;
    HAL_I2C_DeInit(&hi2c4);
    HAL_I2C_Init(&hi2c4);
    HAL_I2CEx_ConfigAnalogFilter(&hi2c4, I2C_ANALOGFILTER_ENABLE);
    HAL_I2CEx_ConfigDigitalFilter(&hi2c4, 0);
    HAL_I2C_Slave_Receive_IT(&hi2c4, &ipmbb_input_buffer[0], IPMB_BUFF_SIZE);
    i2c4_current_state = I2C_MODE_SLAVE;
    
    if ( (tx_ret_val == HAL_OK) && (semphr_timeout != pdFALSE) )
        return IPMB_SEND_DONE;
    else
    	return IPMB_SEND_FAIL;
}


int ipmc_ios_ipmba_read(uint8_t *MsgPtr)
{
    int i;
    // Length zero means no message received
    if(i2c1_recv_len > 0)
    {
        MsgPtr[0] = ipmb_addr << 1;
        for(i=0; (i < i2c1_recv_len); i++)
            MsgPtr[i+1] = ipmba_input_buffer[i];

        i2c1_recv_len = 0;
        HAL_I2C_Slave_Receive_IT(&hi2c1, &ipmba_input_buffer[0], IPMB_BUFF_SIZE);
        ipmc_ios_printf("\n");
        return i+1;
    }
    else
        return 0;
}

int ipmc_ios_ipmbb_read(uint8_t *MsgPtr )
{
    int i;
    // Length zero means no message received
    if(i2c4_recv_len > 0)
    {
        MsgPtr[0] = ipmb_addr << 1;
        for(i=0; (i < i2c4_recv_len); i++)
            MsgPtr[i+1] = ipmbb_input_buffer[i];

        i2c4_recv_len = 0;
        HAL_I2C_Slave_Receive_IT(&hi2c4, &ipmbb_input_buffer[0], IPMB_BUFF_SIZE);
        ipmc_ios_printf("\n");
        return i+1;
    }
    else
        return 0; 
}

// Holds the ipmb_0_msg_receiver_task until the peripheral task releases the semaphore (it will release when a message is received)
void ipmc_ios_ipmb_wait_input_msg(void)
{
    xSemaphoreTake (ipmb_rec_semphr, portMAX_DELAY);
}


void HAL_I2C_MasterTxCpltCallback(I2C_HandleTypeDef *hi2c)
{
	static BaseType_t xHigherPriorityTaskWoken;
	xHigherPriorityTaskWoken = pdFALSE;    
    
    if( (hi2c->Instance==I2C1) && (i2c1_current_state==I2C_MODE_MASTER) )
	{
        xSemaphoreGiveFromISR(ipmba_send_semphr, &xHigherPriorityTaskWoken);
		portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
	}
 	else if ( (hi2c->Instance==I2C4) && (i2c4_current_state==I2C_MODE_MASTER) )
    {
        xSemaphoreGiveFromISR(ipmbb_send_semphr, &xHigherPriorityTaskWoken);
 		portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
 	}
}

void HAL_I2C_SlaveRxCpltCallback(I2C_HandleTypeDef *hi2c)
{
	// this callback is not being used as the interrupt from the Slave mode is returning in the ERROR trigger and not in the EVENT trigger
}

void HAL_I2C_ErrorCallback(I2C_HandleTypeDef *hi2c)
{
  /** Error_Handler() function is called when error occurs.
    * 1- When Slave don't acknowledge it's address, Master restarts communication.
    * 2- When Master don't acknowledge the last data transferred, Slave don't care in this example.
    */
	static BaseType_t xHigherPriorityTaskWoken;
	xHigherPriorityTaskWoken = pdFALSE;

	if(hi2c->Instance==I2C1 && i2c1_current_state == I2C_MODE_SLAVE)
	{
		i2c1_recv_len = IPMB_BUFF_SIZE - hi2c->XferSize;
		if (i2c1_recv_len>0)
		{
			xSemaphoreGiveFromISR(ipmb_rec_semphr, &xHigherPriorityTaskWoken);
		}
		portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
	}
	else if (hi2c->Instance==I2C4 && i2c4_current_state == I2C_MODE_SLAVE)
    {
		i2c4_recv_len = IPMB_BUFF_SIZE - hi2c->XferSize;
		if (i2c4_recv_len>0)
		{
			xSemaphoreGiveFromISR(ipmb_rec_semphr, &xHigherPriorityTaskWoken);
		}
		portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
	}
	else if ((i2c1_current_state == I2C_MODE_MASTER) || (i2c4_current_state == I2C_MODE_MASTER))
    {
        //ipmc_ios_printf("I2C MASTER SEND FAIL - ISR ERROR CALLED");
    }

}

// Interrupt function Handlers for the I2C peripherals
void I2C1_EV_IRQHandler(void)
{
    HAL_I2C_EV_IRQHandler(&hi2c1);
}

void I2C1_ER_IRQHandler(void)
{
    HAL_I2C_ER_IRQHandler(&hi2c1);
}

void I2C4_EV_IRQHandler(void)
{
    HAL_I2C_EV_IRQHandler(&hi2c4);
}

void I2C4_ER_IRQHandler(void)
{
    HAL_I2C_ER_IRQHandler(&hi2c4);
}

/*
 * Control the Blue Led
 */
void ipmc_ios_blue_led(int blue_led_state)
{
	if (blue_led_state == 0)
		HAL_GPIO_WritePin(GPIOE, GPIO_PIN_1, GPIO_PIN_RESET); // LED OFF
	else
		HAL_GPIO_WritePin(GPIOE, GPIO_PIN_1, GPIO_PIN_SET); // LED ON
}

/*
 * Implementation of printf. It uses library developed by Marco Paland (info@paland.com)
 * under MIT license available at github.com/mpaland/printf
 */
void ipmc_ios_printf(const char* format, ...)
{
	va_list args;
	
	xSemaphoreTake(printf_mutex, portMAX_DELAY);
	
	va_start( args, format );
	vprintf_( format, args );
	va_end( args );
	
	xSemaphoreGive(printf_mutex);
}

void _putchar(char character)
{
	HAL_UART_Transmit(&huart3, &character, 1, 1000);
	//outbyte(character);
}
